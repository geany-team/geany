#include "geany.h"
#include "plugindata.h"


int main()
{
    /*
     * t64 migration:
     * - The ABI needs to be renamed for t64 on 32-bit architectures with 64-bit
     *   time_t, but isn't really necessary for others
     * - x32 and i386 do not have abi changes, so exclude those specifically
     *   (see https://bugs.debian.org/1067427#10)
     * - The t64 token should be dropped once the SONAME of the library changes,
     *   or in our case, once the ABI number shifts from 18688
     */
    const char *t64_token = "";
#if (!defined(__i386__)) || (defined(__x86_64__) && defined(__ILP32__))
    if (GEANY_ABI_VERSION == 18688 && sizeof(time_t) == 8 && sizeof(void*) == 4) {
        t64_token = "-t64";
    }
#endif

    printf("GEANY_ABI=geany-abi%s-%d\nGEANY_API=geany-api-%d\n",
           t64_token, GEANY_ABI_VERSION, GEANY_API_VERSION);
    return 0;
}
